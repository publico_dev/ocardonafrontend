---

## Proyecto Omar Cardona - Frontend para AlMundo.com

Este proyecto es una app desarrollada para la prueba de almundo.com. Tiene como objetivo poder visualizar la lista de hoteles en entorno web y movil, con oción de filtro por estrellas y por nombre.

## Puesta en marcha

La puesta en marcha de la aplicación es muy rapida:

1. Descargar el repositorio: git clone https://oacardona@bitbucket.org/publico_dev/ocardonafrontend.git
2. Ingresar al directorio del proyecto descargado
3. Ejecutamos: npm install
4. Ejecutamos: ng serve

Si todo salió bien, ya debería estar disponible en http://localhost:4200/
