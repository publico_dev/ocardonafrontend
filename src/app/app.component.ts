import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';
  private endPoint = 'http://127.0.0.1:8000';
  data: any = {};
  dataFilter: any = {};
  inpSearch : any;
  radStars : any;
  numStars : any [];

  constructor(private http: Http){
    this.inpSearch = '';
    this.radStars = '';
    this.numStars = new Array(1,2,3,4,5);

    this.getApiHotels();
  	this.getHotels();
  	
  }

  getApiHotels(){
  	return this.http.get(this.endPoint + '/hotels')
  		.map((res: Response) => res.json() )
  }

  getHotels(){
    this.getApiHotels().subscribe( data => {
      console.log(data);
      this.data = data;
      this.dataFilter = data;
    })
  }


  getApiHotelsFilter(){
    var urlEndPoint = '/hotels/filter/?'
    if(this.inpSearch)
      urlEndPoint = urlEndPoint + '&name=' + this.inpSearch
    if(this.radStars)
      urlEndPoint = urlEndPoint + '&stars=' + this.radStars

    if(urlEndPoint == '/hotels/filter/?'){
      this.getHotels();
      return;
    }

    return this.http.get(this.endPoint + urlEndPoint )
      .map((res: Response) => res.json() )
  }

  getHotelsFilter(){
    this.getApiHotelsFilter().subscribe( data => {
      console.log(this.radStars);
      this.dataFilter = data;
    })
  }



}
